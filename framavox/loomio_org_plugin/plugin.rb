module Plugins
  module LoomioOrg
    class Plugin < Plugins::Base
      setup! :loomio_org_plugin do |plugin|
        plugin.enabled = true

        LOOMIO_ORG_PAGES = %w(marketing)
        LOOMIO_ORG_PAGES.each { |page| plugin.use_page page, "pages##{page}" }
        plugin.use_class 'controllers/pages_controller'

        plugin.use_page('/', 'pages#marketing')

        plugin.use_static_asset :assets, 'pages/index.scss', standalone: true
        plugin.use_static_asset :assets, 'pages/marketing.coffee', standalone: true
        plugin.use_static_asset_directory :"assets/pages/images", standalone: true
      end
    end
  end
end
